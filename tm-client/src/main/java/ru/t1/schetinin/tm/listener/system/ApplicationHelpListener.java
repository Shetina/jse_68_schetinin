package ru.t1.schetinin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.schetinin.tm.api.model.IListener;
import ru.t1.schetinin.tm.event.ConsoleEvent;
import ru.t1.schetinin.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    @NotNull
    @Autowired
    protected AbstractListener[] listeners;

    @NotNull
    public static final String NAME = "help";

    @NotNull
    public static final String DESCRIPTION = "Show command list.";

    @NotNull
    public static final String ARGUMENT = "-h";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[HELP]");
        for (@NotNull final AbstractListener listener : listeners) {
            if (listener.getArgument() != null && !listener.getArgument().isEmpty())
                System.out.println(listener.getArgument() + " - " + listener.getDescription());
        }
        for (@NotNull final AbstractListener listener : listeners) {
            System.out.println(listener.getName() + " - " + listener.getDescription());
        }
    }

}