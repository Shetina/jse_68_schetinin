package ru.t1.schetinin.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.schetinin.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['database.url']}")
    private String DBUrl;

    @Value("#{environment['database.username']}")
    private String DBUser;

    @Value("#{environment['database.password']}")
    private String DBPassword;

    @Value("#{environment['database.driver']}")
    private String DBDriver;

    @Value("#{environment['database.dialect']}")
    private String DBDialect;

    @Value("#{environment['database.hbm2ddl_auto']}")
    private String DBHbm2ddlAuto;

    @Value("#{environment['database.show_sql']}")
    private String DBShowSql;

    @Value("#{environment['database.format_sql']}")
    private String DBFormatSql;

    @Value("#{environment['database.second_lvl_cache']}")
    private String DBSecondLvlCache;

    @Value("#{environment['database.factory_class']}")
    private String DBFactoryClass;

    @Value("#{environment['database.use_query_cache']}")
    private String DBUseQueryCache;

    @Value("#{environment['database.use_min_puts']}")
    private String dBUseMinPuts;

    @Value("#{environment['database.region_prefix']}")
    private String DBRegionPrefix;

    @Value("#{environment['database.config_file_path']}")
    private String DBConfigFilePath;

}
