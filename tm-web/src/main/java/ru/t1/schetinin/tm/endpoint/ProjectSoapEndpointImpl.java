package ru.t1.schetinin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.schetinin.tm.api.service.dto.IProjectDTOService;
import ru.t1.schetinin.tm.soap.*;

@Endpoint
public class ProjectSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public final static String NAMESPACE = "http://tm.schetinin.t1.ru/soap";

    @Autowired
    private IProjectDTOService projectService;

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(@RequestPayload final ProjectFindAllRequest request) throws Exception {
        return new ProjectFindAllResponse(projectService.findAll());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectAddRequest", namespace = NAMESPACE)
    public ProjectAddResponse add(@RequestPayload final ProjectAddRequest request) throws Exception {
        return new ProjectAddResponse(projectService.add(request.getProject()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse save(@RequestPayload final ProjectSaveRequest request) throws Exception {
        return new ProjectSaveResponse(projectService.update(request.getProject()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(@RequestPayload final ProjectFindByIdRequest request) throws Exception {
        return new ProjectFindByIdResponse(projectService.findOneById(request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectExistsByIdRequest", namespace = NAMESPACE)
    public ProjectExistsByIdResponse existsById(@RequestPayload final ProjectExistsByIdRequest request) throws Exception {
        return new ProjectExistsByIdResponse(projectService.findOneById(request.getId()) != null);
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectCountRequest", namespace = NAMESPACE)
    public ProjectCountResponse count(@RequestPayload final ProjectCountRequest request) throws Exception {
        return new ProjectCountResponse(projectService.count());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@RequestPayload final ProjectDeleteByIdRequest request) throws Exception {
        projectService.removeById(request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    public ProjectDeleteResponse delete(@RequestPayload final ProjectDeleteRequest request) throws Exception {
        projectService.remove(request.getProject());
        return new ProjectDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteAllRequest", namespace = NAMESPACE)
    public ProjectDeleteAllResponse clear(@RequestPayload final ProjectDeleteAllRequest request) throws Exception {
        projectService.clear();
        return new ProjectDeleteAllResponse();
    }

}