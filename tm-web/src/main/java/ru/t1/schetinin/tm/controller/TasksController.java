package ru.t1.schetinin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.schetinin.tm.repository.ProjectDTORepository;
import ru.t1.schetinin.tm.repository.TaskDTORepository;

@Controller
public class TasksController {

    @Autowired
    private TaskDTORepository taskRepository;

    @Autowired
    private ProjectDTORepository projectRepository;

    @GetMapping("/tasks")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskRepository.findAll());
        modelAndView.addObject("projectRepository", projectRepository);
        return modelAndView;
    }

}