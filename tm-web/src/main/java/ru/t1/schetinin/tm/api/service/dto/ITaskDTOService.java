package ru.t1.schetinin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.model.dto.TaskDTO;

import java.util.List;

public interface ITaskDTOService {

    @Nullable
    List<TaskDTO> findAll() throws Exception;

    void removeById(@Nullable String id) throws Exception;

    @NotNull
    TaskDTO add(@NotNull TaskDTO model) throws Exception;

    void clear() throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    @Nullable
    TaskDTO findOneById(@NotNull String id) throws Exception;

    void remove(@NotNull TaskDTO model) throws Exception;

    TaskDTO update(@NotNull TaskDTO model) throws Exception;


    int count() throws Exception;

    void changeTaskStatusById(@Nullable String id, @Nullable Status status) throws Exception;

    @NotNull
    TaskDTO create(@Nullable String name) throws Exception;

    @NotNull
    TaskDTO create(@Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    List<TaskDTO> findAllByProjectId(@Nullable String projectId) throws Exception;

    void updateById(@Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    void updateProjectIdById(@Nullable final String id, @Nullable final String projectId) throws Exception;

}
