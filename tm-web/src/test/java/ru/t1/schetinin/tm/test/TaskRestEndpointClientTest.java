package ru.t1.schetinin.tm.test;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.client.ProjectRestEndpointClient;
import ru.t1.schetinin.tm.client.TaskRestEndpointClient;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.marker.IntegrationCategory;
import ru.t1.schetinin.tm.model.dto.ProjectDTO;
import ru.t1.schetinin.tm.model.dto.TaskDTO;

import java.util.List;

@Category(IntegrationCategory.class)
public final class TaskRestEndpointClientTest {

//    @NotNull
//    public final static String PROJECT1_NAME = "Project 1";
//
//    @NotNull
//    public final static String PROJECT1_DESCRIPTION = "Description 1";
//
//    @NotNull
//    public final static String TASK1_NAME = "Task 1";
//
//    @NotNull
//    public final static String TASK1_DESCRIPTION = "Description 1";
//
//    @NotNull
//    public final static String TASK2_NAME = "Task 2";
//
//    @NotNull
//    public final static String TASK2_DESCRIPTION = "Description 2";
//
//    @NotNull
//    public final static String TASK3_NAME = "Task 3";
//
//    @NotNull
//    public final static String TASK3_DESCRIPTION = "Description 3";
//
//    @NotNull
//    private final TaskRestEndpointClient taskEndpointClient = TaskRestEndpointClient.client();
//
//    @NotNull
//    private final ProjectRestEndpointClient projectEndpointClient = ProjectRestEndpointClient.client();
//
//    @NotNull
//    private ProjectDTO project1 = new ProjectDTO(PROJECT1_NAME, PROJECT1_DESCRIPTION);
//
//    @NotNull
//    private TaskDTO task1 = new TaskDTO(TASK1_NAME, TASK1_DESCRIPTION);
//
//    @NotNull
//    private TaskDTO task2 = new TaskDTO(TASK2_NAME, TASK2_DESCRIPTION);
//
//    @NotNull
//    private TaskDTO task3 = new TaskDTO(TASK3_NAME, TASK3_DESCRIPTION);
//
//    private long count = 0;
//
//    @Before
//    public void before() throws Exception {
//        count = taskEndpointClient.findAll().size();
//        projectEndpointClient.add(project1);
//        task1.setProjectId(project1.getId());
//        taskEndpointClient.add(task1);
//        taskEndpointClient.add(task2);
//    }
//
//    @After
//    public void after() throws Exception {
//        taskEndpointClient.delete(task1);
//        taskEndpointClient.delete(task2);
//        taskEndpointClient.delete(task3);
//        projectEndpointClient.delete(project1);
//    }
//
//    @Test
//    public void testFindAll() throws Exception {
//        @Nullable final List<TaskDTO> tasks = taskEndpointClient.findAll();
//        Assert.assertNotNull(tasks);
//        Assert.assertEquals(count + 2, tasks.size());
//        for (@NotNull TaskDTO task : tasks) {
//            Assert.assertNotNull(taskEndpointClient.findById(task.getId()));
//        }
//    }
//
//    @Test
//    public void testFindAllByProjectId() throws Exception {
//        @Nullable final List<TaskDTO> tasks = taskEndpointClient.findAllByProjectId(task1.getProjectId());
//        Assert.assertNotNull(tasks);
//        Assert.assertEquals(1, tasks.size());
//        for (@NotNull TaskDTO task : tasks) {
//            Assert.assertNotNull(taskEndpointClient.findById(task.getId()));
//        }
//    }
//
//    @Test
//    public void testAdd() throws Exception {
//        @Nullable TaskDTO task = taskEndpointClient.add(task3);
//        Assert.assertNotNull(task);
//        Assert.assertEquals(task3.getName(), task.getName());
//        Assert.assertEquals(task3.getDescription(), task.getDescription());
//    }
//
//    @Test
//    public void testSave() throws Exception {
//        @Nullable TaskDTO task = taskEndpointClient.findById(task1.getId());
//        task.setStatus(Status.IN_PROGRESS);
//        Assert.assertNotNull(taskEndpointClient.save(task));
//        @Nullable TaskDTO task2 = taskEndpointClient.findById(task1.getId());
//        Assert.assertEquals(task.getStatus(), task2.getStatus());
//    }
//
//    @Test
//    public void testFindById() throws Exception {
//        @Nullable TaskDTO task = taskEndpointClient.findById(task1.getId());
//        Assert.assertEquals(TASK1_NAME, task.getName());
//        Assert.assertEquals(TASK1_DESCRIPTION, task.getDescription());
//    }
//
//    @Test
//    public void testExistsById() throws Exception {
//        Assert.assertTrue(taskEndpointClient.existsById(task1.getId()));
//        Assert.assertFalse(taskEndpointClient.existsById(task3.getId()));
//    }
//
//    @Test
//    public void testCount() throws Exception {
//        Assert.assertEquals(count + 2, taskEndpointClient.count());
//    }
//
//    @Test
//    public void testDeleteById() throws Exception {
//        taskEndpointClient.deleteById(task1.getId());
//        Assert.assertNull(taskEndpointClient.findById(task1.getId()));
//    }
//
//    @Test
//    public void testDelete() throws Exception {
//        taskEndpointClient.delete(task1);
//        Assert.assertNull(taskEndpointClient.findById(task1.getId()));
//    }

}