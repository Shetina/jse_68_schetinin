package ru.t1.schetinin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.t1.schetinin.tm.dto.model.UserDTO;

@Repository
public interface IUserDTORepository extends IDTORepository<UserDTO> {

    @NotNull
    UserDTO findByLogin(@NotNull final String login);

    @NotNull
    UserDTO findByEmail(@NotNull final String email);

    boolean existsByLogin(@NotNull String login);

    boolean existsByEmail(@NotNull String email);

}
